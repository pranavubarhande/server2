const express = require('express')
const app = express()

app.get('/', (req, res) => {
    // res.send("pranav website")
    res.sendFile('frontend/index.html', {root: __dirname });
})
app.listen(3000, () => {
    console.log('my app is running');
})